/*
para compilar en linux hay que darle al compilador las librerias requeridas.
g++ testImage.cpp -o testImage $(pkg-config --libs allegro-5.0 allegro_image-5.0 allegro_dialog-5.0 allegro_font-5.0 allegro_ttf-5.0)
*/


#include <iostream>
#include <string>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_native_dialog.h>

#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>


#define len(x) (sizeof(x)/sizeof(x[0]))

#define ancho 800
#define alto 600


using namespace std;

//#bolean function
int login(string clientes[], string claves[]); 
int plataCaja(int billetes[]);
void menuConsultas(int uRetiro[], int saldos[], int pos, ALLEGRO_BITMAP  *,ALLEGRO_EVENT_QUEUE *);
void menuPrincipal(string clientes[], string claves[], string dir[], int saldos[], int billetes[],int uRetiro[], int servicios[][3]);
void menuRetiro(string clientes[], int saldos[], int billetes[],int uRetiro[], int pos,ALLEGRO_BITMAP  *,ALLEGRO_EVENT_QUEUE *);
void retiro(int saldos[], int uRetiro[], int billetes[], int pos, int total);
void menuPagos(int saldos[], int servicios[][3], int pos,ALLEGRO_EVENT_QUEUE *);
void menuModificar(string claves[], string dir[], int pos, ALLEGRO_BITMAP  *,ALLEGRO_EVENT_QUEUE *);
void allegroStop(ALLEGRO_BITMAP  *image,ALLEGRO_DISPLAY *display,ALLEGRO_TIMER *timer);
void loadcajero(int dinero[]);
bool validator(int num);
void allegroInit();
void drawMoney(int pagobill[],int flag);
int main(){

	
	string clientes[3]={"pablo","alejo","daniel"};
	string claves[3]={"123","456","789"};
	string dir[3]={"a","b","c"};
	int saldos[3]={200,50,200};
	int billetes[3]={0,0,0};
	int uRetiro[3]={0,0,0};
	int servicios[3][3]={(100,200,300),(400,500,600),(700,800,150)}; //servicios[cliente][servicios]

	menuPrincipal(clientes,claves,dir,saldos,billetes,uRetiro,servicios);

	return 0;
}

//inicializa allegro
void allegroInit(){
	al_init();
	al_init_image_addon();
	al_install_mouse();
	al_init_font_addon(); // initialize the font addon
	al_init_ttf_addon();// initialize the ttf (True Type Font) addon
}
void allegroStop(ALLEGRO_BITMAP  *image,ALLEGRO_DISPLAY *display,ALLEGRO_TIMER *timer){
	al_rest(2);
	al_destroy_display(display);
	al_destroy_bitmap(image);
	al_destroy_timer(timer);
}

//imprime el dinero retirado por el usuario
void drawMoney(int pagobill[],int flag){
	ALLEGRO_BITMAP  *image = NULL;
	int x=100,y=150;

	image=al_load_bitmap("fondo.jpg");
	al_draw_bitmap(image,0,0,0);
	al_flip_display();

	ALLEGRO_FONT *font = al_load_ttf_font("Yahoo.ttf",24,0 );

	if(flag!=0){
		al_draw_text(font, al_map_rgb(255,0,0), 100, 100,ALLEGRO_ALIGN_INTEGER,"Tome su dinero!!");
		al_flip_display();
		cout<<"len(pagobill)= "<<len(pagobill)<<endl;
		for (int i=0; i<=len(pagobill); i++){
			for(int j=0; j<pagobill[i];j++){
				cout<<"pagobil["<<i<<"] ="<<pagobill[i]<<"  i="<<i<<endl;
				if(i==0){
					image = al_load_bitmap("b50.jpg");  
				}
				else if(i==1){
					image = al_load_bitmap("b20.jpg");  
				}
				else if(i==2){
					image = al_load_bitmap("b10.jpg");  
				}
				al_draw_bitmap(image,x,y,0);
				al_flip_display();
				x+=30;
				y+=30;
			}
		}
	}else{
		al_draw_text(font, al_map_rgb(255,0,0), 100, 100,ALLEGRO_ALIGN_INTEGER,"no hay dinero!!");
		al_flip_display();
	}
	al_rest(2);
}

void retiro(int saldos[], int uRetiro[], int billetes[], int pos, int total){
	int pagobill[3]={0,0,0}; 
	int plataCajero=plataCaja(billetes);
	int plataCuenta = saldos[pos]-total;
	int aux = total;
	
	if(plataCajero>=total && plataCuenta>=0){
		while(total){
			if(saldos[0]>0 && total>=50 && billetes[0]>0){
				total-=50;
				billetes[0]-=1;
				pagobill[0]+=1;
				
			}else if(saldos[1]>0 && total>=20 && billetes[1]>0){
				total-=20;
				pagobill[1]+=1;
				billetes[1]-=1;
				
			}else if(saldos[2]>0 && total>=10 && billetes[2]>0){
				total-=10;
				pagobill[2]+=1;
				billetes[2]-=1;
				
			}
		}
		saldos[pos]-=aux;
		uRetiro[pos]=aux;
		drawMoney(pagobill,1);
		cout<<"50: "<<pagobill[0]<<endl<<"20: "<<pagobill[1]<<endl<<"10: "<<pagobill[2]<<endl;    
	}else{
		drawMoney(pagobill,0);
	}
}


void menuRetiro(string clientes[], int saldos[], int billetes[], int uRetiro[], int pos, ALLEGRO_BITMAP  *img,ALLEGRO_EVENT_QUEUE *event_queue){
	int op=0;
	img=al_load_bitmap("img2.jpg");
	al_draw_bitmap(img,0,0,0);
	al_flip_display();
	
	while(1){
			ALLEGRO_EVENT ev;
			al_wait_for_event(event_queue, &ev);
			if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
				 break;
			}
			else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {

				cout<<"X:"<<ev.mouse.x<<" Y:"<<ev.mouse.y<<endl;
				if((ev.mouse.x>83 && ev.mouse.x<255) && (ev.mouse.y>210 && ev.mouse.y<230)){       
					retiro(saldos,uRetiro,billetes, pos, 20);
					break;
				}
				if((ev.mouse.x>110 && ev.mouse.x<275) && (ev.mouse.y>307 && ev.mouse.y<335)){  
					retiro(saldos,uRetiro,billetes, pos, 50);
					break;
				}
				if((ev.mouse.x>82 && ev.mouse.x<245) && (ev.mouse.y>417 && ev.mouse.y<441)){  
					retiro(saldos,uRetiro,billetes, pos, 80);
				}
				if((ev.mouse.x>476 && ev.mouse.x<665) && (ev.mouse.y>203 && ev.mouse.y<232)){  
					retiro(saldos,uRetiro,billetes, pos, 120);
					break;
				}
				if((ev.mouse.x>534 && ev.mouse.x<724) && (ev.mouse.y>312 && ev.mouse.y<338)){  
					retiro(saldos,uRetiro,billetes, pos, 150);
					break;
				}
				if((ev.mouse.x>510 && ev.mouse.x<703) && (ev.mouse.y>423 && ev.mouse.y<447)){  
					retiro(saldos,uRetiro,billetes, pos, 200);
					break;
				}
				if((ev.mouse.x>288 && ev.mouse.x<508) && (ev.mouse.y>515 && ev.mouse.y<536)){  
					int peticion;
					cout<<endl<<"----------- suma diferente ------------"<<endl<<"ingrese la cantidad solicitada:";
					cin>>peticion;
					peticion/=1000;
					if(validator(peticion))retiro(saldos,uRetiro,billetes, pos,peticion);
					break;
					cout<<saldos[pos]<<endl;         
				}
			}
		}
}

void menuConsultas(int uRetiro[], int saldos[], int pos, ALLEGRO_BITMAP  *img,ALLEGRO_EVENT_QUEUE *event_queue){
	int op=0;
	img=al_load_bitmap("img3.jpg");
	al_draw_bitmap(img,0,0,0);
	al_flip_display();

	ALLEGRO_FONT *font = al_load_ttf_font("Yahoo.ttf",24,0 );

	while(1){
			ALLEGRO_EVENT ev;
			al_wait_for_event(event_queue, &ev);
			if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
				 break;
			}
			else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {

				cout<<"X:"<<ev.mouse.x<<" Y:"<<ev.mouse.y<<endl;
				if((ev.mouse.x>107 && ev.mouse.x<274) && (ev.mouse.y>316 && ev.mouse.y<349)){ //saldos 
					al_draw_textf(font, al_map_rgb(0,0,0), 190, 380,ALLEGRO_ALIGN_CENTRE,"$%i",(saldos[pos]*1000)); 
					al_flip_display();     
				}
				if((ev.mouse.x>419 && ev.mouse.x<708) && (ev.mouse.y>186 && ev.mouse.y<225)){ //ultimo retiro
					al_draw_textf(font, al_map_rgb(0,0,0), 470, 250,ALLEGRO_ALIGN_CENTRE,"$%i",(uRetiro[pos]*1000)); 
					al_flip_display();
				}
				if((ev.mouse.x>659 && ev.mouse.x<769) && (ev.mouse.y>551 && ev.mouse.y<577)){  
					break;
				}
				
			}
		}
}

void menuModificar(string claves[], string dir[], int pos, ALLEGRO_BITMAP  *img,ALLEGRO_EVENT_QUEUE *event_queue){
	int op=0;
	img=al_load_bitmap("img4.jpg");
	al_draw_bitmap(img,0,0,0);
	al_flip_display();

	ALLEGRO_FONT *font = al_load_ttf_font("Yahoo.ttf",24,0 );
	cout<<endl<<"-------------- Modificar ------------"<<endl<<endl;
	while(1){
			ALLEGRO_EVENT ev;
			al_wait_for_event(event_queue, &ev);
			if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
				 break;
			}
			else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {

				cout<<"X:"<<ev.mouse.x<<" Y:"<<ev.mouse.y<<endl;
				if((ev.mouse.x>144 && ev.mouse.x<568) && (ev.mouse.y>263 && ev.mouse.y<300)){ //modificar clave 
					cout<<"nueva clave = ";cin>>claves[pos];
					break;
				}
				if((ev.mouse.x>246 && ev.mouse.x<739) && (ev.mouse.y>376 && ev.mouse.y<407)){ //modificar direccion
					cout<<endl<<"nueva direccion= ";cin>>dir[pos];
					break;
				}
				if((ev.mouse.x>69 && ev.mouse.x<174) && (ev.mouse.y>543 && ev.mouse.y<568)){  
					break;
				}
				
			}
		}
}

void menuPagos(int saldos[], int servicios[][3], int pos, ALLEGRO_BITMAP  *img,ALLEGRO_EVENT_QUEUE *event_queue){
	int op=0;
	img=al_load_bitmap("img5.jpg");
	al_draw_bitmap(img,0,0,0);
	al_flip_display();
	int auxSaldo,auxServicios;

	ALLEGRO_FONT *font = al_load_ttf_font("Yahoo.ttf",14,0 );

	while(1){
			ALLEGRO_EVENT ev;
			al_wait_for_event(event_queue, &ev);
			if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
				 break;
			}
			else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {

				cout<<"X:"<<ev.mouse.x<<" Y:"<<ev.mouse.y<<endl;
				if((ev.mouse.x>268 && ev.mouse.x<467) && (ev.mouse.y>244 && ev.mouse.y<278)){ //telefono 
					cout<<"saldos[pos]="<<saldos[pos]<<"    servicios[pos][2]="<<servicios[pos][2]<<endl;
					//break;
					if(saldos[pos]>=servicios[pos][2]){
						auxSaldo=saldos[pos];
						auxServicios=servicios[pos][2]; 
						saldos[pos]-=servicios[pos][2];
						servicios[pos][2]=0;
						al_draw_textf(font, al_map_rgb(255,0,0), 400, 340,ALLEGRO_ALIGN_INTEGER," Factura Cancelada = $%i",auxServicios*1000);
						al_draw_textf(font, al_map_rgb(255,0,0), 400, 360,ALLEGRO_ALIGN_INTEGER," saldo anterior = $%i",auxSaldo*1000);
						al_draw_textf(font, al_map_rgb(255,0,0), 400, 380,ALLEGRO_ALIGN_INTEGER," saldo actual = $%i",saldos[pos]*1000); 
					}else{
						al_draw_text(font, al_map_rgb(255,0,0), 470, 250,ALLEGRO_ALIGN_INTEGER,"Saldo insuficiente"); 
					}
					al_flip_display();
					al_rest(5);
					break;
				}
				if((ev.mouse.x>118 && ev.mouse.x<232) && (ev.mouse.y>340 && ev.mouse.y<366)){ //Agua
					if(saldos[pos]>=servicios[pos][0]){
						auxSaldo=saldos[pos];
						auxServicios=servicios[pos][2]; 
						saldos[pos]-=servicios[pos][0];
						servicios[pos][0]=0;
						al_draw_textf(font, al_map_rgb(255,0,0), 400, 340,ALLEGRO_ALIGN_INTEGER," Factura Cancelada = $%i",auxServicios*1000);
						al_draw_textf(font, al_map_rgb(255,0,0), 400, 360,ALLEGRO_ALIGN_INTEGER," saldo anterior = $%i",auxSaldo*1000);
						al_draw_textf(font, al_map_rgb(255,0,0), 400, 380,ALLEGRO_ALIGN_INTEGER," saldo actual = $%i",saldos[pos]*1000);
					}else{
						al_draw_text(font, al_map_rgb(255,0,0), 240, 350,ALLEGRO_ALIGN_INTEGER,"Saldo insuficiente");
						cout<<"saldo insuficiente";
					}
					al_flip_display();
					al_rest(5);
					break;
				}
				if((ev.mouse.x>197 && ev.mouse.x<372) && (ev.mouse.y>434 && ev.mouse.y<463)){ // energia
					//break;
					if(saldos[pos]>=servicios[pos][1]){ 
						auxSaldo=saldos[pos];
						auxServicios=servicios[pos][2];
						saldos[pos]-=servicios[pos][1];
						servicios[pos][1]=0;
						al_draw_textf(font, al_map_rgb(255,0,0), 400, 340,ALLEGRO_ALIGN_INTEGER," Factura Cancelada = $%i",auxServicios*1000);
						al_draw_textf(font, al_map_rgb(255,0,0), 400, 360,ALLEGRO_ALIGN_INTEGER," saldo anterior = $%i",auxSaldo*1000);
						al_draw_textf(font, al_map_rgb(255,0,0), 400, 380,ALLEGRO_ALIGN_INTEGER," saldo actual = $%i",saldos[pos]*1000);
					}else{
						//cout<<"saldo insuficiente";
						al_draw_text(font, al_map_rgb(0,0,0), 380, 440,ALLEGRO_ALIGN_INTEGER,"Saldo insuficiente"); 
					}    
					al_flip_display();
					al_rest(5);
					break;
				}
				if((ev.mouse.x>667 && ev.mouse.x<743) && (ev.mouse.y>552 && ev.mouse.y<571)){
					break;
				}
				
			}
		}
}


void menuPrincipal(string clientes[], string claves[], string dir[], int saldos[], int billetes[], int uRetiro[],int servicios[][3]){
	int op=2;
	int pos;  //posicion del usuario en los vectores.
	int stop=0;

	allegroInit();
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_BITMAP  *image = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;

	while(op!=3){
		cout<<"-----------   ingrese opcion   -----------"<<endl<<endl<<"1 - cargar cajero"<<endl<<"2 - login"<<endl<<"3 - salir"<<endl<<"opcion: "; cin>>op;
		if(op==1) loadcajero(billetes);
		if(op==2){
			pos=login(clientes,claves);
			//pos=0;
			if(pos !=-1){
				timer = al_create_timer(1.0 / 60);
				event_queue = al_create_event_queue();
				display = al_create_display(ancho,alto);

				al_register_event_source(event_queue, al_get_display_event_source(display));
				al_register_event_source(event_queue, al_get_timer_event_source(timer));
				al_register_event_source(event_queue, al_get_mouse_event_source());
	 
				image = al_load_bitmap("img1.jpg");
				al_draw_bitmap(image,0,0,0);
				al_flip_display();

				while(stop==0){
					ALLEGRO_EVENT ev;
					al_wait_for_event(event_queue, &ev);
					if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
						al_destroy_display(display);
						al_destroy_bitmap(image);
						op=3;
						break;
					}
					else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
						cout<<"X:"<<ev.mouse.x<<" Y:"<<ev.mouse.y<<endl;
						if((ev.mouse.x>238 && ev.mouse.x<445) && (ev.mouse.y>165 && ev.mouse.y<197)){       //retiros
							menuRetiro(clientes, saldos, billetes,uRetiro, pos,image,event_queue);  
							break; 
						}
						else if((ev.mouse.x>324 && ev.mouse.x<616) && (ev.mouse.y>265 && ev.mouse.y<300)){  //consultas
							menuConsultas(uRetiro,saldos,pos,image,event_queue);
							break;
						}
						else if((ev.mouse.x>350 && ev.mouse.x<640) && (ev.mouse.y>358 && ev.mouse.y<395)){  //modificar
							menuModificar(claves,dir,pos,image,event_queue);  
							break;
						}
						else if((ev.mouse.x>255 && ev.mouse.x<409) && (ev.mouse.y>450 && ev.mouse.y<485)){  //pagos
							menuPagos(saldos,servicios,pos,image,event_queue);
							break;
						}
						else if((ev.mouse.x>354 && ev.mouse.x<455) && (ev.mouse.y>555 && ev.mouse.y<580)){  //pagos
							al_destroy_display(display);
							al_destroy_bitmap(image);
							op=3;
							break;
						}
					} 
				}
				al_destroy_bitmap(image);
				al_destroy_display(display);
				al_destroy_timer(timer);
				al_destroy_event_queue(event_queue);
			}
		}
	}
}

//retorna la posicion del cliente
int login(string clientes[], string claves[]){
	int flag=0,pos=-1;
	string name,clave;

	while(flag==0){
		cout<<"*************************************************************************"<<endl;
		cout<<"******************    CAJERO AUTOMATICO   *******************************"<<endl;
		cout<<"*************************************************************************"<<endl;
		cout<<"-----  LOGIN  ------"<<endl;
		cout<<"Nombre: ";cin>>name;
		cout<<"Passwd: ";cin>>clave; cout<<endl;

		for(int i=0; i< len(clientes) && pos<0; i++){
			if(clientes[i].compare(name)==0 && claves[i].compare(clave)==0 ){
				pos=i;
				flag=1;
			}

		}
		(pos!=-1)? cout<<"logueado"<<endl :cout<<"Nombre o contraseña incorrecta"<<endl;

	}
	return pos;
}

int plataCaja(int billetes[]){
	return (billetes[0]*50+billetes[1]*20+billetes[2]*10);
} 


bool validator(int num){
	if(num>0 && num<700 && num%10==0)
		return true;
	return false;
} 

//carga de dinero el cajero.
void loadcajero(int dinero[]){
	cout<<endl<<endl<<"---------------   Cargar cajero   -----------"<<endl<<endl<<"ingrese el numero de billetes de 50.000:";cin>>dinero[0];
	cout<<endl<<"ingrese el numero de billetes de 20.000:";cin>>dinero[1];
	cout<<endl<<"ingrese el numero de billetes de 10.000:";cin>>dinero[2];
	cout<<endl<<endl<<endl<<endl;

}
